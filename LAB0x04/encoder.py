
'''
@file encoder.py
@brief      This file facilitates the interaction with hardware.
@details    This file works with an encoder driver. The user is able to specify
            the specific pins and timer for the relative encoder. A 16-bit timer
            is being used in this file which can easily be changed if user is
            using a different bit timer. The following values are returned in
            this file: encoder position, different in previous and current encoder
            position, and zeros encoder position.
@page page_LAB3   LAB0x03: Incremental Encoders

'''

import pyb

class EncoderDriver: 
    
    '''
    Encoder task.
    
    This class simply returns the position, change in position and difference between a previous and present encoder position. 
    
    '''
    
    def __init__(self, pin1, pin2, tim):
            '''
            @brief      Creates Encoder object.
            @details    A constructor for an encoder driver which allows user 
                        to input arbitrary pins and timer. These are relative
                        and easy to change based on which pins you connect your
                        encoder.
            '''
            ## Encoder Timer
            self.tim= pyb.Timer(3)
            
            #Timer doesn't scale any value and period= max 16-bit number
            self.time= self.tim.init(prescaler=0, period= 65535)
            
            ## 16-bit max number
            self.period= 65535
            
            #Set up pins
            self.pinA6 = pyb.Pin.cpu.A6
            self.pinA7 = pyb.Pin.cpu.A7
            
            ## Two channel objects for pinA6 and pinA7
            self.CH1= self.tim.channel(1, pin = self.pinA6, mode = pyb.Timer.ENC_AB)
            self.CH2= self.tim.channel(2, pin = self.pinA7, mode = pyb.Timer.ENC_AB)
            
            ## Initialize counter based on relative encoder Timer
            self.counter= self.tim.counter()
            
            ## Initialize start position
            self.position= 0
            
            ## Initialize start position
            self.fixed_delta= 0

    def update(self):
        '''
        Runs one iteration of the task. Returns encoder's updated position
        '''
        ## Account for previous counter
        self. previous_counter= self.counter
            
        ## Update counter value
        self.counter= self.tim.counter()
            
        ## Delta magnitudes
        self.mag= abs(self.counter- self.previous_counter)
           
        ## Diffrentiate between good and bad deltas.
        if (self.mag < self.period/2): #Good delta magnitude
                ## Subtract old count to get self.delta
                self.delta = self.counter-self.previous_counter
                self.fixed_delta= self.delta
                
       ## Diffrentiate between good and bad deltas.     
        elif (self.mag > self.period/2):  #Bad delta magnitude
                ## Subtract old count to get self.delta
                self.delta = self.counter-self.previous_counter
                
                ## Taking care of negative delta value
                if self.delta < 0:
                    self.fixed_delta= self.delta + self.period

                ## Taking care of positive delta value
                elif self.delta > 0:
                    self.fixed_delta= self.delta- self.period
        else:
                    self.fixed_delta= 0
            
        ## Add to position which doesn't reset for each rev or when time overflows
        self.position= self.position + self.fixed_delta
            
    def get_position(self):
        '''
        Returns updated encoder position.
        '''
        return self.position

    def set_position(self):
        '''
        Sets encoder position to zero
        '''
        self.position= 0

    def get_delta(self):
        '''
        Returns a fixed delta. The difference between a previous and current position.
        '''
        return self.fixed_delta



