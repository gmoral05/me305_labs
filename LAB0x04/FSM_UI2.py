
'''
@file       FSM_UI2.py
@brief      User interface task as a finite state machine
@details    This file allows for interaction with encoder object from script
            FSM_encoder.py. This task accepts a couple of commands: G or S.
            This task is continuosly waiting for these commands which return
            the relevant encoder metrics. 
@author     Giselle Morales
@page page_LAB4   LAB0x04: Incremental Encoders


'''
# Extend code to facilitate collecting & saving data from your encoderOUTPUT.
#The user should be able to open & run a python file in Spyder that acts as the UI front end
# create a main.py file which accepts & reads the UART characters & returns the 
# encoder data on a .CSV script that includes TIMESTAMPS associated with each encoder sample.

#At the end of data collection, plot data using matplotlib module

# Press 'c' FROM KEY IN SPYDER to begin data collection at a fixed rate.
# Store these vales in ARRAY on Nucleo, & transmit BATCH DATA to laptopUI.

# If the user presses 'G', the Nucleo should begin sampling from encoder. 
# Data collection stopped by pressing 'S'.
# Data collection= complete: generate both an image & .CSV file of data. LABEL IMAGE
import csv
import time
import matplotlib.pyplot as plt
import serial
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)


inv = input('Give me a character: ')
# time.sleep(2)

def sendChar():
    if inv== 'G':
        ser.write(str(inv).encode('ascii'))
        myval = ser.readline()
        str_data = myval.decode('utf-8')
        str_data.strip()
        data_back= str_data.strip().split(',')
        # print (data_back)
        y= int(data_back[0])
        return y
    else:
        ser.write(str(inv).encode('ascii'))
        myval = ser.readline()
        str_data = myval.decode('ascii')
        print (str_data)
        return 0

def returntim():
    if inv== 'G':
        ser.write(str(inv).encode('ascii'))
        myval = ser.readline()
        # time.sleep(1)
        str_data = myval.decode('utf-8')
        str_data.strip()
        data_back= str_data.strip().split(',')
        x= int(data_back[1])
        return x
    else:
        ser.write(str(inv).encode('ascii'))
        myval = ser.readline()
        str_data = myval.decode('ascii')
        print (str_data)
        return 0

if sendChar()> 0:
    f= open('encoderdata.csv', 'w', newline='')
    # field names  
    fields = ['Time', 'Encoder Position (CPR)']
    c= csv.writer(f)
    for x in range(70):
    # for x in len(sendChar()):#could replace with len(data_back)
        print(sendChar())
        print(returntim())
        c.writerow([sendChar(), returntim()])
        
        #Plotting
        plt.plot(returntim(), sendChar())
        plt.xlabel('Encoder Position')
        plt.ylabel ('Time')
        # f.close()
        # ser.close()
else:
    print ('Please enter G to start data collection: ')
    

ser.close()
f.close()

