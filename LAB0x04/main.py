'''
@file       main.py
@brief      Main Script File
@details    Main script file that runs continuously. The only thing that
            happens in this file is initializing tasks and then indefinitely
            running the task loop at a fixed interval. This could be considered
            a "round-robin" scheduler, which is a cooperative multi-tasking
            scheme.This script returns the encoder position relative to time.
            
@page page_LAB4   LAB0x04: Interface Extension

@section page_lab4_src Source Code Access
The source code for LAB4 can be found below

https://bitbucket.org/gmoral05/me305_labs/src/master/LAB0x04/

'''

# Upon recieving relavant character, the 'data collection task' should begin
# sampling from encoder at 5Hz for 10s UNLESS terminated 'S'.
# Data should be stored on the Nucleo for batch transmission back to your
# laptop. use array() module


# import array
# # import shares
# import utime
# from encoder import EncoderDriver
# from pyb import UART    

# tim= range(50)
# vals= range(50)

# # myuart= UART(2)
# # while True:
# #     if myuart.any() != 0:
# #         # if cmd= 
# #         val = myuart.readchar()
# #         myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo') 


import pyb
import utime
from pyb import UART
from encoder import EncoderDriver

# ser =UART(2)

print ('Enter G: ')

# while True:
#     if ser.any() != 0:
#         # if cmd= 
#         val = ser.readchar()
#         # self.ser.write('You sent an ASCII ' + str(val) + ' to the Nucleo') 
#         # print (y)
#         tim= range(100)
#         encodervals= range(100)
#         # self.timee= (self.curr_time - self.start_time)
#         # self.newtim= self.timee +self.timee
#         # self.vals= self.EncoderDriver.get_position()
#         # self.transitionTo(self.S1_DataCollect)
#         # for n in range(len(list_variable)):
#             # while val ==71:
#         if val == 71: #G
#                 for n in range(len(tim)):
#                     # self.tim= (self.curr_time - self.start_time)
#                     # self.vals= self.EncoderDriver.get_position()
#                     # ser.write('{:}, {:}\r\n'.format(tim[n], encodervals[n]))
#                     print('{:}, {:}'.format(tim[n], encodervals[n]))
#                     # self.transitionTo(self.S1_DataCollect)
#                     # self.ser.write(z)
#         elif val == 83:
#                         print ('You stopped data collection.')
#         else:
#             print ('Please re-run & enter G')
#!!!!!!!!!




class TaskDataCollection:
    '''
    Encoder task.
    
    An object of this class encodes characters sent by a TaskUser object. The chosen UI is to return the relevent encoder metric.
    '''

    ## Initialization state
    S0_INIT               = 0
    
    ## Wait for Data collection
    S1_DataCollect        = 1


    def __init__(self, interval, encoder):
        '''
        Creates a encoder task object.
        @param interval A number to specify interval between tasks
        @param encoder An object that is created from an imported encoder class
        '''        
        ## Object of the imported class EncoderDriver
        self.EncoderDriver= encoder
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = int(utime.ticks_ms())# The number of miliseconds since Jan 1. 1970
    
        ## The interval of time, in miliseconds, between runs of the task
        self.interval = int(interval)
    
        ## The "timestamp" for when to run the task next
        self.next_time = int(utime.ticks_add(self.start_time, self.interval))
        
        ## Serial port, creates an object for UART
        self.ser =UART(2)
        

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = int(utime.ticks_ms())
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_DataCollect)
                # print ('''Input G to start data collection: ''')
       
            elif(self.state == self.S1_DataCollect):
                        # Run State 1 Code
                # self.EncoderDriver.set_position()
                self.pos_length= self.EncoderDriver.get_position()
                self.EncoderDriver.update()
                self.transitionTo(self.S1_DataCollect)
                # print (self.EncoderDriver.get_position())
                self.time= (self.curr_time - self.start_time)
                # print (self.pos_length)
                timlist = [self.time for i in range(1)]
                poslist = [self.pos_length for i in range(1)]
                # while True:
                while self.time < 15000:
                    if self.ser.any() != 0:
                            val = self.ser.readchar()
                            # if val == 71 and self.time< 10000: #G
                            if val == 71: #G
                                    for n in range(len(poslist)):
                                         # ser.write('{:}, {:}\r\n'.format(tim[n], encodervals[n]))
                                        print('{:}, {:}'.format(timlist [n], poslist[n]))
                                        self.transitionTo(self.S1_DataCollect)
                                        # if self.time>99999:
                                        #     print ('Data collection stopped')
                                        #     break
    
                            # elif val == 83 and self.time< 10000: #S
                            elif val == 83: #S
                                 for n in range(len(poslist)):
                                        # print ('You stopped data collection.Here is your data: ')
                                        print('{:}, {:}'.format(timlist [n], poslist[n]))
                                        self.transitionTo(self.S1_DataCollect)
                                        break

                            else:
                                print ('10 seconds max hit. Here is your data: ')
                                break
                            break
                    break
                        
                    break
                

            
            self.runs += 1
            
            ## Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

        

### NEW CODE, DELETE AFTER
## Specific pins that the encoder is connected to. Change depending on what pins you used to plug in your encoder.
A6= pyb.Pin.cpu.A6
A7= pyb.Pin.cpu.A7
enc= EncoderDriver(A6, A7, 3)
task2 = TaskDataCollection (2, enc)

while True:   
        task2.run() 

 