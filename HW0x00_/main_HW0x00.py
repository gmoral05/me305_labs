# -*- coding: utf-8 -*-
"""
@file main_HW0x00.py

@mainpage

@section sec_intro Introduction
Welcome to my ME305 main page! Here are all my lab assignments I've worked on this quarter.

@section sec_lab1 Lab1: Fibonacci
This lab deals with taking in a user index input for the fibonacci sequence 
and calculating the relevant integers.

@section sec_HW0x00 HW0x00: FSM_Elevator
This assignment serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator(s).


@author Giselle Morales

@date October 5th, 2020


"""

# Import classes
from FSM_HW0x00_ import MotorDriver, Button, TaskElevator1, TaskElevator2

# This code could be in another file like main.py if you wanted
        
## Motor Objects
Motor_1 = MotorDriver()
Motor_2 = MotorDriver()
## Button Objects for "go to first floor button"
button_1 = Button('PB6')
button_1_2 = Button('PC6')
## Button Objects for "go to second floor button"
button_2 = Button('PB7')
button_2_2 = Button('PC7')
## Sensor Objects for "bottom limit"
first = Button('PB8')
first_2 = Button('PC8')
## Sensor Objects for "top limit"
second = Button('PB9')
second_2 = Button('PC9')
## Task objects
task1 = TaskElevator1(0.1, Motor_1, button_1, button_2, first, second) # Will also run constructor
task2 = TaskElevator2(0.1, Motor_2, button_1_2, button_2_2, first_2, second_2) # Will also run constructor

# To run the task call task1.run() and task2.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run1()
    # task2.run2()
