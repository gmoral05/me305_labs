'''
@file FSM_HW0x00_.py

@brief This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator(s).

@details The user has a two buttons to turn move the elevator up or down.
There are also proximity sensors which tell the user whether they are on the first or second floor.

@image html FSM-1.jpg

@author Giselle Morales

@date October 5, 2020

@page page_HW   HW0x00: FSM_Elevator

@section page_hw_src Source Code Access
The source code for the FSM program can be found below

https://bitbucket.org/gmoral05/me305_labs/src/master/HW0x00_/



'''

from random import choice
import time

class TaskElevator1:
    '''
    @brief      A finite state machine to control elevator 1.
    @details    This class implements a finite state machine to control whether
                an elevator travels to floor_1 or floor_2 based on button pressed.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                    = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN             = 1
    
    ## Constant defining State 2
    S2_STOPPED_ON_FLOOR_1      = 2
    
    ## Constant defining State 3
    S3_MOVING_UP               = 3
    
    ## Constant defining State 4
    S4_STOPPED_ON_FLOOR_2      = 4
    
    
    def __init__(self, interval, Motor_1, button_1, button_2, first, second):
        '''
        @brief      Creates a TaskElevator1 object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor_1 = Motor_1 # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the button_1 object
        self.button_1 = button_1
        
        ## A class attribute "copy" of the button_2 object
        self.button_2 = button_2
        
        ## The sensor object used for bottom limit
        self.first = first
        
        ## The sensor object used for top limit
        self.second = second
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run1(self):
        '''
        @brief      Runs one iteration of the task
        @details    The input runs through various if statments and settles on the appropriate line of code.
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        ## checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.Motor_1.Down()
                self.transitionTo(self.S1_MOVING_DOWN)
                #CLEAR BOTH BUTTONS
                self.button_1.getButtonState(1)
                self.button_2.getButtonState(1)
            
            
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                if self.first.getButtonState(0):
                    self.transitionTo(self.S2_STOPPED_ON_FLOOR_1)
                    self.Motor_1.Stop()
                    # CLEAR BUTTON_1
                    self.button_1.getButtonState(1)
            
            
            elif(self.state == self.S2_STOPPED_ON_FLOOR_1):
                print (str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time- self.start_time))
                # Run State 2 Code
                if self.button_2.getButtonState(0):
                    self.transitionTo(self.S3_MOVING_UP)
                    self.Motor_1.Up()
                    
                    
            elif(self.state == self.S3_MOVING_UP):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.second.getButtonState(0):
                    self.transitionTo(self.S4_STOPPED_ON_FLOOR_2)
                    self.Motor_1.Stop()
                    #CLEAR BUTTON_2
                    self.button_2.getButtonState(1)
            
            elif(self.state == self.S4_STOPPED_ON_FLOOR_2):
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.button_1.getButtonState(0):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor_1.Down()

            else:
                print ('Error. Please re-run program')
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp

    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class TaskElevator2:
    '''
    @brief      A finite state machine to control elevator 2.
    @details    This class implements a finite state machine to control whether
                an elevator travels to floor_1_1 or floor_2_2 based on button pressed.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                    = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN             = 1
    
    ## Constant defining State 2
    S2_STOPPED_ON_FLOOR_1      = 2
    
    ## Constant defining State 3
    S3_MOVING_UP               = 3
    
    ## Constant defining State 4
    S4_STOPPED_ON_FLOOR_2      = 4
    
    
    def __init__(self, interval, Motor_2, button_1_2, button_2_2, first_2, second_2):
        '''
        @brief      Creates a TaskElevator2 object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor_2 = Motor_2 # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the button_1 object
        self.button_1_2= button_1_2
        
        ## A class attribute "copy" of the button_2 object
        self.button_2_2= button_2_2
        
        ## The sensor object used for bottom limit
        self.first_2= first_2
        
        ## The sensor object used for top limit
        self.second_2=second_2
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    

    def run2(self):
        '''
        @brief      Runs one iteration of the task
        @details    The input runs through various if statments and settles on the appropriate line of code.
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        ## checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.Motor_2.Down()
                self.transitionTo(self.S1_MOVING_DOWN)
                #CLEAR BOTH BUTTONS
                self.button_1_2.getButtonState(1)
                self.button_2_2.getButtonState(1)
            
            
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                if self.first_2.getButtonState(0):
                    self.transitionTo(self.S2_STOPPED_ON_FLOOR_1)
                    self.Motor_2.Stop()
                    # CLEAR BUTTON_1
                    self.button_1_2.getButtonState(1)
            
            
            elif(self.state == self.S2_STOPPED_ON_FLOOR_1):
                print (str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time- self.start_time))
                # Run State 2 Code
                if self.button_2_2.getButtonState(0):
                    self.transitionTo(self.S3_MOVING_UP)
                    self.Motor_2.Up()
                    
                    
            elif(self.state == self.S3_MOVING_UP):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.second_2.getButtonState(0):
                    self.transitionTo(self.S4_STOPPED_ON_FLOOR_2)
                    self.Motor_2.Stop()
                    #CLEAR BUTTON_2
                    self.button_2_2.getButtonState(1)
            
            elif(self.state == self.S4_STOPPED_ON_FLOOR_2):
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.button_1_2.getButtonState(0):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor_2.Down()

            else:
                print ('Error. Please re-run program')
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to move elevator up or down. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self, value):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False if value==0 but
                    F.
        @return     A boolean representing the state of the button.
        '''
        
        if value == 0:
            return choice([True, False])
            # return choice([True, False])
        else:
            return False

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Down(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor pulling elevator DOWN')
    
    def Up(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor pulling elevator UP')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor Stopped')

# ## Motor Objects
# Motor_1 = MotorDriver()
# Motor_2 = MotorDriver()
# ## Button Objects for "go to first floor button"
# button_1 = Button('PB6')
# button_1_2 = Button('PC6')
# ## Button Objects for "go to second floor button"
# button_2 = Button('PB7')
# button_2_2 = Button('PC7')
# ## Sensor Object for "bottom limit"
# first = Button('PB8')
# first_2 = Button('PC8')
# ## Sensor Object for "top limit"
# second = Button('PB9')
# second_2 = Button('PC9')
# ## Task object
# task1 = TaskElevator1(0.1, Motor_1, button_1, button_2, first, second) # Will also run constructor
# task2 = TaskElevator2(0.1, Motor_2, button_1_2, button_2_2, first_2, second_2) # Will also run constructor

# # To run the task call task1.run() and task2.run() over and over
# for N in range(10000000): #Will change to   "while True:" once we're on hardware
#     task1.run1()
#     # task2.run2()
    
    
