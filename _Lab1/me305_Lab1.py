# -*- coding: utf-8 -*-
'''
@file me305_Lab1.py

@brief This file contains a function which calculates the fibonacci sequence

@details This file takes a user index input and calculates the corresponding 
fibonacci number. Strings and negative numbers are accounted for when calculating.

@author Giselle Morales

@date September 24, 2020

@page page_fib Fibonacci

@section ppage_fib_src Source Code Access
The spurce code for the Fibonacci program can be found below

https://bitbucket.org/gmoral05/me305_labs/src/master/


''' 

# Import in-built python lru_cache to add memoization
from functools import lru_cache

# Set a while loop to account for a string input.
while True:
    try:
        ## Set user input to a variable named idx and works with it in the script
        idx = int(input('Please enter an index to calculate fibonacci sequence: '))
        break
    except ValueError:
        print("Re-run the program and input a positive integer.")
    

# Call out lru_cache and set a maxsize to 10000 to not double calculate numbers below that treshold
## Maxsize sets a size of integer to not double calculate numbers below that treshold
@lru_cache(maxsize= 10000)

# Define a function fib with a parameter named idx
## idx is a user input parameter
def fib (idx):
    '''
    @brief         This function runs when a user index is inputted.
    @details       The input runs through various if statments and settles on the appropriate line of code.
    @param idx     An integer representing the user input for an index
    '''
    if idx == 0:
        return 0
    elif idx == 1:
        return 1
    elif idx == 2:
        return 1
    elif idx >= 3:
        return fib(idx-1) + fib(idx-2)
    else: 
        pass

    
if __name__ == '__main__':
    for idx in range(0,idx+1):
        print (fib(idx))
    if idx < 0:
        print ('Re-run the program and enter a positive integer.')



