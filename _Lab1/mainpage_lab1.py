## @file mainpage.py
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Welcome to my ME305 main page! Here are all my lab assignments I've worked on this quarter.
#
#  @section sec_lab1 Lab1
#  This lab deals with taking in a user index input for the fibonacci sequence 
#  and calculating the relevant integers.
#
#  @author Giselle Morales
#
#
#  @date September 24, 2020
#