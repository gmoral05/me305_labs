
'''
@file       FSM_encoder.py
@brief      This file implementats a finite-state-machine using Python. The 
            file works with an encoer.py file which calls the update() method
            at a regular interval.
@details    An EncoderDriver object is created and depending on the metrics of 
            the encoder the user is using, this file updates at the relative 
            amount of seconds to ensure no encoder ticks are missed (aka- the 
            file counts all the encoder ticks). Here, the update() method is 
            run every 0.18 seconds which is dependednt on the pulses per second
            of your encoder and 1/2 the period of the timer you are using. This
            script utilizes a 16-bit timer. Calculation: pps/(period/2)= sec
@author     Giselle Morales
@page page_LAB   LAB0x03: Incremental Encoders

@section page_lab3_src Source Code Access
The source code for the FSM program can be found below

https://bitbucket.org/gmoral05/me305_labs/src/master/Lab%202/

'''

import shares
import pyb
import utime
from encoder import EncoderDriver
from pyb import UART


class TaskEncoder:
    '''
    Encoder task.
    '''
    ## Initialization state
    S0_INIT       = 0
    
    ## Wait for encoder movement
    S1_UPDATE     = 1

    def __init__(self, interval, encoder):
        '''
        Creates a encoder task object.
        @param interval A number to specify interval between tasks
        @param encoder An object that is created from an imported encoder class
        '''
        ## Object of the imported class EncoderDriver
        self.EncoderDriver= encoder

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = int(utime.ticks_ms())# The number of miliseconds since Jan 1. 1970
    
        ## The interval of time, in miliseconds, between runs of the task
        self.interval = int(interval)
    
        ## The "timestamp" for when to run the task next
        self.next_time = int(utime.ticks_add(self.start_time, self.interval))
        
        ## Relevant serial port, creates an object for UART
        self.ser= UART(2)


    def run(self):
        '''
        Runs one iteration of the task
        '''       
        self.curr_time = int(utime.ticks_ms())  #updating the current timestamp
        
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_UPDATE)
                # self.EncoderDriver.update()
                # print (str(self.EncoderDriver.update()))
                # print ('Encoder Initialized' + ' Time {:0.2f} '.format(self.curr_time - self.start_time))
            
            elif(self.state == self.S1_UPDATE):
                # Run State 1 Code
                self.EncoderDriver.get_delta()
                # self.EncoderDriver.get_position()
                self.EncoderDriver.update()
                self.transitionTo(self.S1_UPDATE)
                # print (self.EncoderDriver.get_position())
                # print (self.EncoderDriver.get_delta())
                # print ('Encoder updating' + ' Time {:2.4f} '.format(utime.ticks_diff(self.curr_time,self.start_time)))          
                if shares.cmd:
                    shares.resp = self.UI(shares.cmd)
                    shares.cmd = None
        else:
            print ('Error: Please check hardware')
            pass
            
            self.runs += 1
            
            ## Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
   
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

      
    def UI(self, cmd):
        '''
        Return desired line of code dependent on whether z,p, or d was pressed
        '''
        if cmd == 122: #z
            self.EncoderDriver.set_position()
            print ('The encoder has been zeroed')
            resp= 1
            return resp
        elif cmd == 112: #p
            self.EncoderDriver.get_position()
            print ('The encoders position is ' + str(self.EncoderDriver.get_position()))
            resp= 2
        elif cmd == 100: #d
            self.EncoderDriver.get_delta()
            print ('The difference is: ' + str(self.EncoderDriver.get_delta()))
            resp= 3
        else:
            resp= print ('Please re-run the program & enter a valid letter')
        return resp
    
