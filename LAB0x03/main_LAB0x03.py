'''
@file       main_LAB0x03.py
@brief      Main Script File
@details    Main script file that runs continuously. The only thing that
            happens in this file is initializing tasks and then idefinitely
            running the task loop at a fixed interval. This could be considered
            a "round-robin" scheduler, which is a cooperative multi-tasking
            scheme.
            
            Each task should be specified in a seperate file using a Python 
            class called. The class must contain a function called @c run that
            performs one single iteration of the task.
'''
import shares
import pyb
from encoder import EncoderDriver
from FSM_encoder import TaskEncoder
from FSM_UI import TaskUI


## Specific pins that the encoder is connected to. Change depending on what pins you used to plug in your encoder.
A6= pyb.Pin.cpu.A6
A7= pyb.Pin.cpu.A7

## Creates encoder object specifying which pins the encoder is plugged into and what timer the encoder uses.
enc= EncoderDriver(A6, A7, 3)

## Encoder task encodes characters sent from user task
task1 = TaskEncoder (0.001, enc)

## User interface task, works with serial port
task2 = TaskUI (0.001, enc)

while True:
        task1.run()    
        task2.run()  

