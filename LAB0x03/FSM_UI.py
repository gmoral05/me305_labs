
'''
@file       FSM_UI.py
@brief      User interface task as a finite state machine
@details    This file allows for interaction with encoder object from script
            FSM_encoder.py. This task accepts a short set of commands: z,p, or d.
            This task is continuosly waiting for these commands which return
            the relevant encoder metrics. 
@author     Giselle Morales
@page page_LAB   LAB0x03: Incremental Encoders

@section page_lab3_src Source Code Access
The source code for the FSM program can be found below

https://bitbucket.org/gmoral05/me305_labs/src/master/Lab%202/
'''

import shares
import pyb
import utime
from pyb import UART
from encoder import EncoderDriver


class TaskUI:
    '''
    Encoder task.
    
    An object of this class encodes characters sent by a TaskUser object. The chosen UI is to return the relevent encoder metric.
    '''

    ## Initialization state
    S0_INIT               = 0
    
    ## Wait for encoder movement
    S1_WAIT_FOR_CHAR      = 1
    
    ## Wait for encoder movement
    S2_READ_CHAR          = 2

    def __init__(self, interval, encoder):
        '''
        Creates a encoder task object.
        @param interval A number to specify interval between tasks
        @param encoder An object that is created from an imported encoder class
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Object of the imported class EncoderDriver
        self.encoder= encoder
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = int(utime.ticks_ms())# The number of miliseconds since Jan 1. 1970
    
        ## The interval of time, in miliseconds, between runs of the task
        self.interval = int(interval)
    
        ## The "timestamp" for when to run the task next
        self.next_time = int(utime.ticks_add(self.start_time, self.interval))
        
        ## Serial port, creates an object for UART
        self.ser =UART(2)
        

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                print ('''
Please enter one of the following 3 lowercase letters to issue the command:
                    z | Zero the encoder position
                    p | Print out the encoder position
                    d | print out the encoder delta                   
                      ''')
       
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                # Run State 1 Code             
                if self.ser.any(): 
                    shares.cmd = self.ser.readchar()
                    self.transitionTo(self.S2_READ_CHAR)
                
            elif(self.state == self.S2_READ_CHAR):
                # Run State 2 Code
                if shares.resp:
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    # print (shares.resp)
                    shares.resp = None
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    
            else:
                print ('Error: Please check hardware')
                pass
            
            self.runs += 1
            
            ## Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState


  