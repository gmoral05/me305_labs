
'''
@file               Task_FrontEnd.py
@brief              User interface task that takes in user input for a proportional
                    gain.
@details            This file interacts directly with Task_Backend.py. It recieves
                    data like time and rpm from a motor which were calulcated using
                    a control script. The data is then plotted and the data is stored
                    in a CSV file. This file takes an input for proportional
                    gain which begins the data collection in other scripts.
@author             Giselle Morales
@page page_LAB6     LAB0x06: MotorControl


'''
# Extend code to facilitate collecting & saving data from your encoderOUTPUT.
#The user should be able to open & run a python file in Spyder that acts as the UI front end
# create a main.py file which accepts & reads the UART characters & returns the 
# encoder data on a .CSV script that includes TIMESTAMPS associated with each encoder sample.

#At the end of data collection, plot data using matplotlib module

# Press 'c' FROM KEY IN SPYDER to begin data collection at a fixed rate.
# Store these vales in ARRAY on Nucleo, & transmit BATCH DATA to laptopUI.

# If the user presses 'G', the Nucleo should begin sampling from encoder. 
# Data collection stopped by pressing 'S'.
# Data collection= complete: generate both an image & .CSV file of data. LABEL IMAGE
import sys
import numpy as np
import csv
import time
import matplotlib.pyplot as plt
import serial
## Creates serial port to send data
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)


Kp = int(input('Input a proportional gain Kp to begin : '))
while True:
    if Kp <= 9 and Kp > 0:
        break
    elif Kp !=9: # does not equal 
            print ('Re-run program & enter a value for Kp less than 1 but geater than 0: ')
            sys.exit() 
ser.write(repr(Kp).encode('utf-8'))

Ω_ref= 350
# Ω_ref = float(input('Input Desired Velocity Ω_ref (RPM): '))
# while True:
#     if Ω_ref <= 500 and Ω_ref > 0:
#         break
#     elif Ω_ref !=1: # does not equal 
#             print ('Re-run program & enter a value for Kp less than 1 but geater than 0: ')
#             sys.exit() 
# # Sending input over to Task_Backend
# ser.write('{:}, {:}'.format(Kp,Ω_ref).encode('ascii'))

start_time = time.time()

def return_tim():
            '''
            Sends the user input to through the serial port and reads the data sent
            back through UART. The data being recieved is the time elapsed.
            '''
            if Kp >0 :
                myval = ser.readline()
                # time.sleep(2)
                str_data = myval.decode('utf-8')
                str_data.strip()
                data_back= str_data.strip().split(',')
                print (data_back)
                y_1= int(data_back[0])
                # x= int(data_back[1])
                return y_1
            else:
                pass
        
def return_Ω():
            '''
            Sends the user input to through the serial port and reads the data sent
            back through UART. The data being recieved is the encoder position.
            '''
            if Kp >0:
                myval = ser.readline()
                # time.sleep(2)
                str_data = myval.decode('utf-8')
                str_data.strip()
                data_back= str_data.strip().split(',')
                # print (data_back)
                x_1= int(float(data_back[1]))
                return x_1
            else:
                pass
     
def return_Ω_ref():
            '''
            Sends the user input to through the serial port and reads the data sent
            back through UART. The data being recieved is the encoder position.
            '''
            if Kp >0:
                myval = ser.readline()
                # time.sleep(2)
                str_data = myval.decode('utf-8')
                str_data.strip()
                data_back= str_data.strip().split(',')
                # print (data_back)
                x_2= int(float(data_back[2]))
                return x_2
            else:
                pass
   
   
f= open('encoderdata.csv', 'w', newline='')
c= csv.writer(f)

for x in range(200):
    curr_time = time.time() 
    if (curr_time- start_time) < 1:
        if return_tim()> 0: 
            print('{:0.2f} seconds'.format(curr_time- start_time))        
            c.writerow([return_tim(), return_Ω(), return_Ω_ref()])       
            
ser.close()
f.close()

x_col = []
y_col = []
yy_col = []
with open('encoderdata.csv','r') as csvfile:
    plots= csv.reader(csvfile, delimiter= ',')
    for row in plots:
        x_col.append(int(row[0]))
        y_col.append(int(row[1]))
        yy_col.append(int(row[2]))

plt.plot(x_col,y_col,'-r', x_col, yy_col, '-b')
plt.xlabel('Time (Miliseconds)')
plt.ylabel('Motor Speed (RPM)')
plt.title('Proportional Tunning on Motor Speed')
plt.legend()
plt.show()



