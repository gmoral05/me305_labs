'''
@file shares.py
@brief A container for all the inter-task variables
@author Giselle Morales
@page page_LAB6 MotorControl
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

## The Kp value sent from the Backend Task to the Control Task
Kp       = 1

## The Ω_ref value sent from the Backend Task to the Control Task
Ω_ref    = 0

## The Ω value sent from the Backend Task to the Control Task
Ω        = 0

## The signal to go or no-go from the Backend Task to the Control Task to start Control Task
GO       = False

## The L value sent from the Backend Task to the Control Task
L        = 0

## The signal to end from the Backend Task to the Control Task to end Control Task
END      = False