
'''
@file               encoder.py
@brief              This file facilitates the interaction with hardware.
@details            This file works with an encoder driver. The user is able to specify
                    the specific pins and timer for the relative encoder. A 16-bit timer
                    is being used in this file which can easily be changed if user is
                    using a different bit timer. The following values are returned in
                    this file: encoder position, difference in previous and current encoder
                    position, and zeros encoder position. The encoder position is 
                    converted to degrees using: (1000 cycles/rev)*(4 ticks/cycle)*(4)*(1 rev/360 degrees).
                    This yields 44.4 ticks/degree. This script is also able to calculate
                    motor speed by using encoder ticks to RPM.
                    What is updated in this script is the gear ratio to reflect the new encoder
                    being used, and the subsequent values that are affected by this.
@page page_LAB6     LAB0x06: MotorControl

'''

import pyb

class EncoderDriver: 
    
    '''
    Encoder Driver.
    
    This class simply returns the position, change in position and difference between a previous and present encoder position. 
    
    '''
    
    def __init__(self, pin1, pin2, tim):
            '''
            @brief      Creates Encoder object.
            @details    A constructor for an encoder driver which allows user 
                        to input arbitrary pins and timer. These are relative
                        and easy to change based on which pins you connect your
                        encoder.
            @param pin1           A pyb.Pin object for the encoder object
            @param pin2           A pyb.Pin object for the encoder object
            @param tim            A timer object for the encoder object
            '''
            ## Encoder Timer
            self.tim= pyb.Timer(4)
            
            #Timer doesn't scale any value and period= max 16-bit number
            self.time= self.tim.init(prescaler=0, period= 65535)
            
            ## 16-bit max number
            self.period= 65535
            
            #Set up pins
            self.pinB6 = pyb.Pin.cpu.B6
            self.pinB7 = pyb.Pin.cpu.B7
            
            ## Two channel objects for pinA6 and pinA7
            self.CH1= self.tim.channel(1, pin = self.pinB6, mode = pyb.Timer.ENC_AB)
            self.CH2= self.tim.channel(2, pin = self.pinB7, mode = pyb.Timer.ENC_AB)
            
            ## Initialize counter based on relative encoder Timer
            self.counter= self.tim.counter()
            
            ## Initialize start position
            self.position= 0
            
            ## Initialize start position
            self.fixed_delta= 0

    def update(self):
        ''' Runs one iteration of the task. Calculates encoder's updated position which is dependent on encoder period.'''
        ## Account for previous counter
        self. previous_counter= self.counter
            
        ## Update counter value
        self.counter= self.tim.counter()
            
        ## Delta magnitudes
        self.mag= abs(self.counter- self.previous_counter)
           
        ## Diffrentiate between good and bad deltas.
        if (self.mag < self.period/2): #Good delta magnitude
                ## Subtract old count to get self.delta
                self.delta = self.counter-self.previous_counter
                # Converts ticks to degrees
                self.fixed_delta= (self.delta)
                
       ## Diffrentiate between good and bad deltas.     
        elif (self.mag > self.period/2):  #Bad delta magnitude
                ## Subtract old count to get self.delta
                self.delta = (self.counter-self.previous_counter)
                
                ## Taking care of negative delta value
                if self.delta < 0:
                    self.fixed_delta= self.delta + (self.period)

                ## Taking care of positive delta value
                elif self.delta > 0:
                    self.fixed_delta= self.delta- (self.period)
        else:
                    self.fixed_delta= 0
            
        ## Add to position which doesn't reset for each rev or when time overflows
        self.position= self.position + self.fixed_delta
            
    def get_position(self):
        '''This Method returns updated encoder position.'''

        return self.position

    def set_position(self, updatedPos):
        '''This Method sets encoder position to whatever position is inputted'''
        '@param updatedPos        An input to set a position'
        self.position= updatedPos

    def get_delta(self):
        '''This method returns a fixed delta. The difference between a previous and current position.'''
        return self.fixed_delta

    def ticks_RPM(self, interval):
        '''This method returns the speed of a motor by converting encoder ticks to RPM.
        '''
        self.speed= (self.fixed_delta/ (4*1000*4))/(interval/60000)
        return self.speed

