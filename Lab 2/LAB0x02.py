
'''
@file LAB0x02.py

@brief This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary and a physical
LED.

@details The user has a button 

two buttons to turn move the elevator up or down.
There are also proximity sensors which tell the user whether they are on the first or second floor.

@image html FSM_LAB2.jpg

@author Giselle Morales

@date October 10, 2020

@page page_LAB   LAB0x02: FSM_LED_TriangleWave

@section page_lab2_src Source Code Access
The source code for the FSM program can be found below

https://bitbucket.org/gmoral05/me305_labs/src/master/Lab%202/



'''
import utime
import pyb


class BlinkVirtual:
    
    import utime
    '''
    @brief      A finite state machine to control a virtual LED.
    @details    This class implements a finite state machine to control a 
                "virtual" LED light to print "ON" and "OFF" within a given 
                interval.
    '''
    ## Constant defining State 0
    S0_INIT                    = 0
    ## Constant defining State 1
    S1_ON                      = 1
    ## Constant defining State 2
    S2_OFF                     = 2
    
    def __init__(self, interval):
        '''
        @brief      Creates a BlinkVirtual object.
        '''
        self.state = self.S0_INIT
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = int(utime.ticks_ms()/1000)# The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval)
    
        ## The "timestamp" for when to run the task next
        self.next_time = int(utime.ticks_add(self.start_time, self.interval))
    
    def run1(self):
        '''
        @brief      Runs one iteration of the task
        @details    The in-built function utime runs through all three states (INIT, ON, OFF). 
                    The LED is first initialized then switches to LED OFF & LED ON
                    dependent on whether the time value is odd or even. 
        '''
        self.curr_time = int(utime.ticks_ms()/1000)   #updating the current timestamp
        
        ## checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print ("LED Initialized")
                # print ("LED Initialized" + ' Time {:0.2f}'.format(self.curr_time- self.start_time))
                self.transitionTo(self.S1_ON)
       
            elif(self.state == self.S1_ON):
                # Run State 1 Code
                if self.Button(0):
                    self.transitionTo(self.S2_OFF)
                  
            elif(self.state == self.S2_OFF):
                # Run State 2 Code
                if self.Button(0):
                    self.transitionTo(self.S1_ON)
                    
            else:
                print ('Error. Please re-run program')
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
            self.y = (self.curr_time- self.start_time)

    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

    def Button(self, runs):
        '''
        @brief      Runs one iteration of the task
        @details    The Virtual LED switches from "LED OFF" & "LED ON"
                    dependent on whether the time value is odd or even. Here,
                    the number of runs is keep below 10,000.
        '''  
        
        if (self.y < 100000):
            if (self.runs % 2) == 0:
                    print ('LED OFF' + ' Time {:0.2f}'.format(self.curr_time- self.start_time))             
            else:
                    print ('LED ON' + '  Time {:0.2f}'.format(self.curr_time- self.start_time))                         
        
class Blink_Real:
    
    import utime
    '''
    @brief      A finite state machine to physical LED on a NUCLEO board.
    @details    This class implements a finite state machine to control a
                physical LED. The LED follows a traingle wave form, meaning,
                it increases +15 brightness every run until it hits a max 
                brightness at 100 at 10 seconds, where the LED proceeds to 
                turn off.
    '''
    
    ## Constant defining State 0
    S0_INIT                    = 0
    ## Constant defining State 1
    S1_ON                      = 1
    ## Constant defining State 2
    S2_Increase                = 2
    
    def __init__(self, interval):
        '''
        @brief      Creates a Blink_real object.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = int(utime.ticks_ms()/1000)# The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval)
    
        ## The "timestamp" for when to run the task next
        self.next_time = int(utime.ticks_add(self.start_time, self.interval))

        ## A counter increasing brightness
        self.bright= 0

        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        self.tim2 = pyb.Timer(2, freq = 20000)
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        
    def run2(self):
        '''
        @brief      Runs one iteration of the task
        @details    The in-built function utime runs through all three states (INIT, ON, Increase). 
                    The LED is first initialized then increases in brightness
                    by +15 until it hits a max of 100, where it proceeds to turn off.
        '''
        self.curr_time = int(utime.ticks_ms()/1000)    #updating the current timestamp
        
        ## checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_ON)
                print ('LED Initialized')
                # print ('LED Initialized' + ' Time {:0.2f} '.format(self.curr_time - self.start_time))
                
            
            elif(self.state == self.S1_ON):
                # Run State 1 Code
                self.PinSet()
                self.t2ch1.pulse_width_percent(self.bright)
                # print('Brightness: {:0.2f}'.format(self.bright) + ' Time {:0.2f} '.format(self.curr_time - self.start_time))
                self.Brightness()
                self.transitionTo(self.S2_Increase)
            
            
            elif(self.state == self.S2_Increase):
                # Run State 2 Code
                self.PinSet()
                self.t2ch1.pulse_width_percent(self.bright)
                # print('Brightness: {:0.2f}'.format(self.bright) + ' Time {:0.2f} '.format(self.curr_time - self.start_time))
                self.Brightness()
                self.transitionTo(self.S1_ON)

            else:
                print ('Error. Please re-run program')
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
            self.y = (self.curr_time- self.start_time)
    
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

    def PinSet (self):
            '''
            @brief      Creats Pinset object
            @param pin  A pin object that the button is connected to
            '''
            self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
            self.tim2 = pyb.Timer(2, freq = 20000)
            self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
            
    def Brightness (self):
            '''
            @brief Increases physical LED brightness.
            '''
            if self.bright < 100:
                    self.bright += 12.5
    
            else:
                    self.bright = 0    


# task1 = BlinkVirtual(1) # Will also run constructor
# task2 = Blink_Real(1) # Will also run constructor

# # To run the task call task1.run() and task2.run() over and over
# for N in range(100000000): #Will change to   "while True:" once we're on hardware
#     task1.run1()
#     task2.run2()
    



