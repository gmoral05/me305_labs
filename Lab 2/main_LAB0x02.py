# -*- coding: utf-8 -*-
"""
@file main_LAB0x02.py

@mainpage

@section sec_intro Introduction
Welcome to my ME305 main page! Here are all my lab assignments I've worked on this quarter.

@section sec_lab1 LAB0x01: Fibonacci
This lab deals with taking in a user index input for the fibonacci sequence 
and calculating the relevant integers.

@section sec_lab2 LAB0x02: FSM_LED_TriangleWave
This lab deals with creating a FSM running 2 tasks at the same time. Task 1 consist of blinking a "virtual" LED which simply prints
"LED OFF" and "LED ON" at a fixed interval. The second task physically changes the LED brightness according to a triangular waveform.
Here, the brightness of the LED increases by +15 every run until it hits a max of 100 (at 10 seconds), where, it goes back to a value of zero.

@section sec_HW0x00 HW0x00: FSM_Elevator
This assignment serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator(s).



@author Giselle Morales

@date September 24th, 2020


"""
# Import classes
from LAB0x02 import BlinkVirtual, Blink_Real

task1 = BlinkVirtual(1) # Will also run constructor
task2 = Blink_Real(1) # Will also run constructor

# # To run the task call task1.run() and task2.run() over and over
for N in range(100000000): #Will change to   "while True:" once we're on hardware
    task1.run1()
    task2.run2()
    



