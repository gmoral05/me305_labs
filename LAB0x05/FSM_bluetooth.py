'''
@file       FSM_bluetooth.py
@brief      This file implementats a finite-state-machine using Python. The 
            file works with a bluetooth.py file which uses UART as a form of communication.
@details    A BluetoothDriver object is created and depending on the user input
            frequency, this script send back relevant information while also
            blinking the LED light ON & OFF depending on the input frequency.A
            counter is used to count to relevant seconds.

@author     Giselle Morales
@page page_LAB5   LAB0x05: Bluetooth LED Control

'''

import utime
import pyb
from pyb import UART
from bluetooth import BluetoothDriver


class Task_BLE:
    '''
    Bluetooth task.
    '''
    ## Initialization state
    S0_INIT                 = 0
    
    ## Wait for encoder movement
    S1_Read_INPUTS          = 1
    
        ## Wait for encoder movement
    S2_Write_to_UART        = 2

    def __init__(self, interval, bluetooth):
        '''
        Creates a encoder task object.
        @param interval A number to specify interval between tasks
        @param encoder An object that is created from an imported bluetooth class
        '''
        ## Object of the imported class EncoderDriver
        self.BluetoothDriver= bluetooth

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = int(utime.ticks_ms())# The number of miliseconds since Jan 1. 1970
    
        ## The interval of time, in miliseconds, between runs of the task
        self.interval = int(interval)
    
        ## The "timestamp" for when to run the task next
        self.next_time = int(utime.ticks_add(self.start_time, self.interval))
        
        ## Relevant serial port, creates an object for UART
        self.uart= UART(3, 9600)
        
        
    def run(self):
        '''
        Runs one iteration of the task
        '''       
        self.curr_time = int(utime.ticks_ms())  #updating the current timestamp
        
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_Read_INPUTS)
                # self.BluetoothDriver.read_UART()
                # self.BluetoothDriver.read_IN_WAITING()
                # self.BluetoothDriver.led_ON()
                # self.BluetoothDriver.led_OFF()
                # print (str(self.EncoderDriver.update()))
                # print ('Encoder Initialized' + ' Time {:0.2f} '.format(self.curr_time - self.start_time))
            
            elif(self.state == self.S1_Read_INPUTS):
                ## Read from UART
                self.IN= self.BluetoothDriver.read_UART()
                print ('here')
                if self.BluetoothDriver.get_value() in range (1,11):
                    # self.IN= self.BluetoothDriver.read_UART() #Read from UART
                    self.val= int(self.BluetoothDriver.get_value())
                    print (self.val)
                    self.transitionTo(self.S2_Write_to_UART)
                else:
                    self.transitionTo(self.S1_Read_INPUTS)
                
                self.count = 0
            elif(self.state == self.S2_Write_to_UART):
                # Run State 2 Code
                self.count_updated =1 + self.count
                # print ('line 108')
                ## Writes back to UART with the inputted frequency
                ## LED blinks at preferred frequency 
                self.freq= 1000/(int(self.val))

                self.BluetoothDriver.write_UART()
                while self.count_updated <= 1000:
                    if self.val > 0: ## Sub for IN-waiting()
                        self.BluetoothDriver.led_OFF()
                        self.count_updated =1 + self.count_updated
                        print (self.count_updated)
                        for n in range (1, self.val):
                            while self.count_updated== self.freq*n:
                                n += 1
                                print (self.freq*n)
                                self.BluetoothDriver.led_ON()
                                self.count_updated =1 + self.count_updated
                                # n += 1
                                
                                
                        # if self.count_updated < self.freq: #z
                        #         print ('LED ON')
                        #         # self.BluetoothDriver.led_OFF()
                        #         # self.BluetoothDriver.led_ON()
                        #         self.count_updated =1 + self.count_updated
                        
                        #         print (self.count_updated)
                        #         if self.count_updated == self.freq:
                        #         # if self.count_updated in range (int(self.freq), 1000, int(self.freq)):
                        #             # print (self.freq)
                        #             self.BluetoothDriver.led_ON()
                        #             # self.count_updated =1
                        #             self.transitionTo(self.S2_Write_to_UART)
                        # else:
                        #         self.BluetoothDriver.led_OFF()
                        #         self.count= 0



                        ## Checks if characters are waiting in UART
                        # self.BluetoothDriver.read_IN_WAITING() #Check if any characters are waiting
                        # self.transitionTo(self.S2_Write_to_UART)
                        # while self.uart.in_waiting > 0:
                        #     self.transitionTo(self.S1_Read_INPUTS)
            
            else:
                    print ('Error: Please check hardware')
                    pass
                
            self.runs += 1
                
                    ## Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    

pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
BLE= BluetoothDriver(pinA5)
task1 = Task_BLE (10, BLE)

while True:
        task1.run() 
            
            