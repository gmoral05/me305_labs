'''
@file bluetooth.py
@brief      This file facilitates the interaction with hardware.
@details    This file works with a bluetooth driver. The user is able to specify
            the specific pins for the relative bluetooth. The following values 
            are returned in this file: read from, write to, and check
            if any characters or waiting on the UART.This script also allows 
            you to turn on and off an LED light.

@page page_LAB5   LAB0x05: Bluetooth LED Control

'''

import pyb
from pyb import UART

class BluetoothDriver: 
    
    '''
    Encoder task.
    
    This class simply creates a pin object for the relevant pin the bluetooth
    is connected to.
    
    '''
    
    def __init__(self, pin1):
            '''
            @brief      Creates Bluetooth object.
            @details    A constructor for an bluetooth driver that allows forv
                        reading from, writing to, and checking if any characters
                        are waiting on the UART. Turns off & on an LED.
            '''

            #Timer doesn't scale any value and period= max 16-bit number
            self.uart =UART(3, 9600)
            
            #Set up pins
            self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
            
            # ## Initialize counter based on relative encoder Timer
            # self.counter= self.tim.counter()
            
            ## Initialize start position
            self.position= 0
            
            ## Initialize start position
            self.fixed_delta= 0
            self.read= 0
            self.read_in_waiting= 0

    def read_UART(self):
        '''
        @brief      Runs task while true.
        @details    A constructor for an bluetooth driver that allows forv
                    reading from, writing to, and checking if any characters
                    are waiting on the UART. Turns off & on an LED.
        '''
           
        ## Reads from UART
        if self.uart.any() != 0: #Good delta magnitude
                ## Read from UART
                self.read = int(self.uart.readline())
                
            
    def write_UART(self):
        '''
        @brief     Reads and write frequency information to user.
        '''
        self.read_= str(self.read)
        self.uart.write(str(' LED blinking at ' + self.read_ + 'Hz').encode('ascii'))

    def get_value(self):
        '''
        @brief     Returns updated user input through the phone.
        '''
        return self.read
    
    # def read_IN_WAITING(self):
    #     '''
    #     Reads integers in waiting
    #     '''
    #     while self.uart.in_waiting > 0: # waiting certain size of data
    #         return self.uart.in_waiting


    def led_ON(self):
        '''
        Returns an led OFF command. 
        '''
        self.pinA5.high()
    
    def led_OFF(self):
        '''
        Returns an led ON command.
        '''
        self.pinA5.low()
    
# pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
# BLE= BluetoothDriver(pinA5)
# task1 = BluetoothDriver (BLE)

# while True:
#         task1.read_UART() 
