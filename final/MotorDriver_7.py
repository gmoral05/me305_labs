
'''
@file Motor_Driver.py

@brief              This file controls the rotation of a motor using pulse width modulation
                    ranging in values from -100 to 100. Motor is counterclockwise when positive
                    values are entered and clockwise when negative values are entered.It facilitates
                    the interaction with hardware.

@details            When running this script in putty, the user calls in the nSLEEP pin, and
                    two input pins for half bridge 1 & 2 along with their relative channels. 
                    The timer is called in last. The user is then able to enable the motors 
                    and set a duty cycle that ranges from -100 to 100. The positive values
                    turn the motors counterclockwise while the negative values turn the values
                    clockwise.

@author             Giselle Morales

@date               November 11, 2020

@page page_LAB6     LAB0x06: MotorControl

'''
import pyb

class MotorDriver:
    'This class implements a motor driver for the ME 405 board'
    
    def __init__ (self, pin_nSLEEP, pin_IN1, CH_IN1, pin_IN2, CH_IN2, timer):
        '''Created a motor driver by initiating pins & turning the motor off for saftey."
        @param pin_nSLEEP        A pyb.Pin object to use as the enable pin
        @param pin_IN1           A pyb.Pin object to use as the input to half bridge 1
        @param CH_IN1            A tim.channel object used to set the channel
                                 and mode for CH_IN1.
        @param pin_IN2           A pyb.Pin object to use as the input to half bridge 2
        @param CH_IN2            A tim.channel object used to set the channel
                                 and mode for CH_IN2.
        @param timer             A pyb.Timer object to use for PWM generation on
                                 pin_IN1 and pin_IN2'''
        # Creating sleep pin object
        self.pin_SLEEP= pyb.Pin(pin_nSLEEP, pyb.Pin.OUT_PP)
        # Creating sleep pin object
        self.pin_IN1_= pyb.Pin(pin_IN1, pyb.Pin.OUT_PP)
        # Creating sleep pin object
        self.pin_IN2_= pyb.Pin(pin_IN2, pyb.Pin.OUT_PP)
        # Creating timer object
        self.tim= pyb.Timer(timer, freq= 20000)
        # Two channel objects for pinA6 and pinA7
        self.timch1= self.tim.channel(CH_IN1,mode = pyb.Timer.PWM, pin = pin_IN1)
        self.timch2= self.tim.channel(CH_IN2,mode = pyb.Timer.PWM, pin = pin_IN2)
            
    def enable(self):
        '''This method enables the motor.'''
        self.pin_SLEEP.high()
        # print ('Enabling Motor')
        
    def disable(self):
        '''This method disables the motor for safety.'''
        self.pin_SLEEP.low()
        # print ('Disabling Motor') 
    
    def set_duty(self, duty):
        # print ('Enabling Motor')
        '''This method sets the duty cycle to be sent to the motor to the given
        level. Positive values cause effort in one direction, negative values
        in the opposite direction. Drives the motor.
        @param duty    A signed integer holding the duty cycle of the PWM signal 
                      sent to the motor'''
        if duty > 0 and duty <= 100:
            self.timch1.pulse_width_percent(duty)
            self.timch2.pulse_width_percent(0)
        elif duty <0 and duty >= -100:
            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(abs(duty))
        elif duty ==0:
            self.timch1.pulse_width_percent(duty)
            self.timch2.pulse_width_percent(duty)
        else:
            print ('Error. Duty cylce out of range. Enter a value below 100')
            self.timch1.pulse_width_percent(0)
        
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if__name__== '__main__' block will only run when the script
    # is executed as a standalone program. If the script is imported as a module the
    # code block will not run
    
    # Create the pin objects used from interfacing with the motor driver
    pin_nSLEEP = None;
    pin_IN1    = None;
    pin_IN2    = None;    
    
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3);
    
    CH_IN1     = timer.channel(1,mode = pyb.Timer.PWM, pin = pin_IN1)
    CH_IN2     = timer.channel(2,mode = pyb.Timer.PWM, pin = pin_IN1)
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_nSLEEP, pin_IN1, CH_IN1, pin_IN2, CH_IN2, timer)
    
    # Enable the motor driver
    moe.enable()
    
    
    # Set the duty cycle to 10 percent
    moe.set_duty(10)


        