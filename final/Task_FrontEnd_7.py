
'''
@file               Task_FrontEnd.py
@brief              User interface task that takes in user input for a proportional
                    gain.
@details            This file interacts directly with Task_Backend.py. It recieves
                    data like time and rpm from a motor which were calulcated using
                    a control script. The data is then plotted and the data is stored
                    in a CSV file. This file takes an input for proportional
                    gain which begins the data collection in other scripts.
@author             Giselle Morales
@page page_LAB6     LAB0x06: MotorControl


'''
# Extend code to facilitate collecting & saving data from your encoderOUTPUT.
#The user should be able to open & run a python file in Spyder that acts as the UI front end
# create a main.py file which accepts & reads the UART characters & returns the 
# encoder data on a .CSV script that includes TIMESTAMPS associated with each encoder sample.

#At the end of data collection, plot data using matplotlib module

# Press 'c' FROM KEY IN SPYDER to begin data collection at a fixed rate.
# Store these vales in ARRAY on Nucleo, & transmit BATCH DATA to laptopUI.

# If the user presses 'G', the Nucleo should begin sampling from encoder. 
# Data collection stopped by pressing 'S'.
# Data collection= complete: generate both an image & .CSV file of data. LABEL IMAGE
import sys
import numpy as np
import csv
import time
import matplotlib.pyplot as plt
import serial
## Creates serial port to send data
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)


Kp = int(input('Input a proportional gain Kp to begin : '))
while True:
    if Kp <= 30 and Kp > 0:
        break
    elif Kp !=30: # does not equal 
            print ('Re-run program & enter a value for Kp less than 1 but geater than 0: ')
            sys.exit() 
ser.write(repr(Kp).encode('utf-8'))

start_time = time.time()

def return_tim():
            '''
            Sends the user input to through the serial port and reads the data sent
            back through UART. The data being recieved is the time elapsed.
            '''
            if Kp >0 :
                myval = ser.readline()
                # time.sleep(2)
                str_data = myval.decode('utf-8')
                str_data.strip()
                data_back= str_data.strip().split(',')
                print (data_back)
                y_1= int(data_back[0])
                # x= int(data_back[1])
                return y_1
            else:
                pass
        
def return_Ω():
            '''
            Sends the user input to through the serial port and reads the data sent
            back through UART. The data being recieved is the encoder position.
            '''
            if Kp >0:
                myval = ser.readline()
                # time.sleep(2)
                str_data = myval.decode('utf-8')
                str_data.strip()
                data_back= str_data.strip().split(',')
                # print (data_back)
                x_1= int(float(data_back[1]))
                return x_1
            else:
                pass
     
def return_Ω_ref():
            '''
            Sends the user input to through the serial port and reads the data sent
            back through UART. The data being recieved is the encoder position.
            '''
            if Kp >0:
                myval = ser.readline()
                # time.sleep(2)
                str_data = myval.decode('utf-8')
                str_data.strip()
                data_back= str_data.strip().split(',')
                # print (data_back)
                x_2= int(float(data_back[2]))
                return x_2
            else:
                pass

def return_deg_ref():
            '''
            Sends the user input to through the serial port and reads the data sent
            back through UART. The data being recieved is the encoder position.
            '''
            if Kp >0:
                myval = ser.readline()
                # time.sleep(2)
                str_data = myval.decode('utf-8')
                str_data.strip()
                data_back= str_data.strip().split(',')
                # print (data_back)
                x_3= int(float(data_back[3]))
                return x_3
            else:
                pass
def return_deg():
            '''
            Sends the user input to through the serial port and reads the data sent
            back through UART. The data being recieved is the encoder position.
            '''
            if Kp >0:
                myval = ser.readline()
                # time.sleep(2)
                str_data = myval.decode('utf-8')
                str_data.strip()
                data_back= str_data.strip().split(',')
                # print (data_back)
                x_4= int(float(data_back[4]))
                return x_4
            else:
                pass
   
f= open('encoderdata.csv', 'w', newline='')
c= csv.writer(f)

for x in range(200):
    curr_time = time.time() 
    if (curr_time- start_time) < 3.10:
        if return_tim()>= 0: 
            print('{:0.2f} seconds'.format(curr_time- start_time))        
            c.writerow([return_tim(), return_Ω(), return_Ω_ref(), return_deg_ref(), return_deg()])       
            
ser.close()
f.close()

tim_col = []
speed_col = []
speedref_col = []
degref_col = []
deg_col = []
with open('encoderdata.csv','r') as csvfile:
    plots= csv.reader(csvfile, delimiter= ',')
    for row in plots:
        tim_col.append(int(row[0]))
        speed_col.append(int(row[1]))
        speedref_col.append(int(row[2]))
        degref_col.append(int(row[3]))
        deg_col.append(int(row[4]))


# To make the example complete, plot the data as a subplot.
plt.figure(1)

# Velocity first
plt.subplot(2,1,1)
plt.plot(tim_col,speedref_col, tim_col, speed_col)
plt.ylabel('Velocity [RPM]')

# Then position
plt.subplot(2,1,2)
plt.plot(tim_col,degref_col,tim_col, deg_col)
plt.xlabel('Time [s]')
plt.ylabel('Position [deg]')


