'''
@file               ClosedLoopController.py
@brief              This file facilitates the interaction with hardware.
@details            This file works with an encoder and motor drivers. It acts as
                    a closedloop controller by taking in a proportional gain
                    and refernce values and calculating an actuating signal. It 
                    also sets the proportional gain value and gets the proportional
                    gain value.
@page page_LAB6     LAB0x06: MotorControl

'''

from encoder import EncoderDriver
from MotorDriver import MotorDriver

class ClosedLoop:
    '''
    Creates a ClosedLoop class.
    '''

    def __init__(self, encoder, MotorDriver):
        '''
        Creates a ClosedLoop object.
        @param interval         A number to specify interval between tasks
        @param encoder          An object that is created from an imported encoder class
        '''
        ## Object of the imported class EncoderDriver
        self.EncoderDriver= encoder
        ## Object of the imported class EncoderDriver
        self.MotorDriver= MotorDriver
        

    def update(self,Ω_ref, Ω,def_ref, deg):
        '''
        Updates the actuating signal and returns it.
        @param Kp A         number to specify interval between tasks
        @param Ω_ref        An object that is created from an imported encoder class
        @param Ω            An object that is created from an imported encoder class
        '''    
        # Computes and returns the actuation value based on measured and reference values
        L = ((self.Kp)*(Ω_ref- Ω)) + (0.1*(def_ref- deg))
        if L > 100:
            return 100
        elif L < -100:
            return -100
        else:
            return L
    
    def get_Kp(self):
        '''
        Returns proportional gain value.
        ''' 
        return self.Kp
    
    def set_Kp(self, Kp_IN):
        '''
        Sets proportional control gain in the script
        @param Kp_IN        A number to specify a new proportional gain.
        ''' 
        self.Kp = Kp_IN
    

    
