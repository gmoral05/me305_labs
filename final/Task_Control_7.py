'''
@file               Task_Control.py
@brief              This file implementats a finite-state-machine using Python. The 
                    file works with an encoder.py, MotorDriver.py, and ClosedLoopController.py
@details            A FSM task that performs control on motor speed. Works alongside
                    a shares.py file which triggers performance steps in this script.
                    Also works alongside Task_Backend.py, Task_FrontEnd.py and a
                    main.py file. This FSM performs calculations specific to closed
                    loop control. It frequency updates velocity depending on user inputs
                    from other scripts.
@author             Giselle Morales
@page page_LAB6     LAB0x06: MotorControl

'''
import pyb
from encoder import EncoderDriver
from MotorDriver import MotorDriver
from ClosedLoopController import ClosedLoop
import utime
import shares

class Task_Control:
    '''
    Control task.
    '''
    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for user input from Backend Task
    S1_WAIT_FOR_GO      = 1
    
    ## Start Closed Loop Control
    S2_CLOSED_CONTROL   = 2

    def __init__(self, encoder, MotorDriver, ClosedLoopController):
        '''
        Creates a Control task object.
        @param interval         An encoder driver object that is imported.
        @param encoder          An motor driver object that is imported.
        @param encoder          An ClosedLoopController driver object that is imported.
        '''
        ## Object of the imported class EncoderDriver
        self.EncoderDriver= encoder
        
        ## Object of the imported class MotorDriver
        self.MotorDriver= MotorDriver

        ## Object of the imported class Driver
        self.ClosedLoop= ClosedLoopController

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = int(utime.ticks_ms())# The number of miliseconds since Jan 1. 1970
    
        ## The interval of time, in miliseconds, between runs of the task
        self.interval = int(30)
    
        ## The "timestamp" for when to run the task next
        self.next_time = int(utime.ticks_add(self.start_time, self.interval))
        
        # PWM Level
        self.level= 0

    def run(self):
        '''
        Runs one iteration of the task
        '''       
        self.curr_time = int(utime.ticks_ms())  #updating the current timestamp
        
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.EncoderDriver.set_position(0)
                self.transitionTo(self.S1_WAIT_FOR_GO)

            
            elif(self.state == self.S1_WAIT_FOR_GO):
                # Run State 1 Code
                if shares.GO== False: # No go signal from Task_Backend
                    pass
                if shares.GO== True: # Go signal from Task_Backend
                    self.MotorDriver.enable()
                    self.EncoderDriver.update()
                    self.transitionTo(self.S2_CLOSED_CONTROL)
                    self.ClosedLoop.set_Kp(shares.Kp)
                    

                    
            elif(self.state == self.S2_CLOSED_CONTROL):
                # Run State 2 Code
                if shares.END== False:
                    self.EncoderDriver.update()
                    self.EncoderDriver.update()
                    shares.deg= self.EncoderDriver.ticks_deg(self.EncoderDriver.get_position())
                    # Send calculated motor speed back to Task_Backend using shares
                    shares.Ω= self.EncoderDriver.ticks_RPM(self.interval)
                    # Update PWM Level
                    self.level= self.ClosedLoop.update(shares.Ω_ref, shares.Ω. shares.def_ref, shares.deg)
                    self.MotorDriver.set_duty(self.level)
                    # print('This is L: ' + str(self.level))
                    shares.L= self.level
                    self.transitionTo(self.S2_CLOSED_CONTROL)
                elif shares.END== True:
                    while(abs(self.EncoderDriver.get_delta() > 0)):
                        # Stop motor and transition to 1
                        self.MotorDriver.disable()
                        self.transitionTo(self.S1_WAIT_FOR_GO)

            ## Increases run count by 1
            self.runs += 1
            
            ## Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
   
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

        

