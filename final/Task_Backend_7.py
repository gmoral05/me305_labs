'''
@file               Task_Backend.py
@brief              This file implementats a finite-state-machine using Python.
@details            This script takes in calculated values from Task_Control 
                    through a shares.py script. It also keeps tracks of time
                    and appends calulated valocity values along with time. This
                    script runs once a number is read through UART. Once
                    the data collection is done, this scipt sends back the
                    collected data back through UART to be read in Task_FrontEnd.py.         
@page page_LAB6     LAB0x06: MotorControl


'''

from Task_Control import Task_Control
from ClosedLoopController import ClosedLoop
import shares
import sys
import pyb
import utime
from pyb import UART
from encoder import EncoderDriver
from MotorDriver import MotorDriver


print('Enter Kp: ')

class TaskDataCollection:
    '''
    Data Collection task.
    '''

    ## Initialization state
    S0_INIT               = 0
    
    ## Wait for Data and Read
    S1_Wait_Read          = 1

    ## Wait for Data collection
    S2_DataCollect        = 2

    ## Wait for Data collection
    S3_SendData           = 3

    def __init__(self, interval, encoder, MotorDriver):
        '''
        Creates a TaskDataCollection object.
        @param interval         A number to specify interval between tasks
        @param encoder          An object that is created from an imported encoderdriver class
        @param encoder          An object that is created from an imported motordriver class
        '''      
        ## Object of the imported class EncoderDriver
        self.EncoderDriver= encoder
        
        ## Object of the imported class MotorDriver
        self.MotorDriver= MotorDriver
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = int(utime.ticks_ms())# The number of miliseconds since Jan 1. 1970
    
        ## The interval of time, in miliseconds, between runs of the task
        self.interval = int(interval)
    
        ## The "timestamp" for when to run the task next
        self.next_time = int(utime.ticks_add(self.start_time, self.interval))
        
        ## Serial port, creates an object for UART
        self.ser =UART(2)
        
        ## Creating Ω array
        self.Ω_array= []
        self.Ω_ref_array= []
        self.deg_ref_array= []
        self.deg_array= []
        self.time_array= []
        self.Ω=0
        shares.deg_ref= 0
        

        self.ref = open('imported_profile.csv');
        
        # ## TEST
        # self.Kp= 5
        self.Ω_ref= 30

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = int(utime.ticks_ms())
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_Wait_Read) 

        
            elif(self.state == self.S1_Wait_Read):
                        # Run State 1 Code
                # self.EncoderDriver.set_position()
                self.pos_length= self.EncoderDriver.get_position()
                self.EncoderDriver.update()
                if self.ser.any() != 0:
                    # print ('STARTED')
                    self.Kp = int(self.ser.read())
                    shares.Kp= self.Kp
                    shares.Ω_ref= self.Ω_ref
                    #Resets time for next run
                    self.start_new_time= 0
                    self.start_new_time= (int(utime.ticks_ms())*self.interval)               
                    self.transitionTo(self.S2_DataCollect)
                    # self.next_time= utime.ticks_add(self.next_time, self.Δt)
                    ## Counter that describes the number of times the task has run
                    self.runs = 0

                    
            elif(self.state == self.S2_DataCollect):
                shares.GO= True
                shares.END= False
            # Run State 2 Code
                if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                    if utime.ticks_diff(self.curr_time, self.start_new_time) >= 10000:
                        self.MotorDriver.disable()
                        shares.END= True
                        # print('End. Motor Disabled')
                        self.transitionTo(self.S3_SendData)
                    else:
                        if shares.Ω > 0:
                            if self.runs< 125:
                                if self.runs >= 0:
                                    self.time_array.append(self.interval*len(self.time_array))
                                    line = self.ref.readline()
                                    (t,v,x) = line.strip().split(',');
                                    self.Ω_ref_array.append(float(v))
                                    self.Ω_array.append(shares.Ω)
                                    self.deg_ref_array.append(float(x))
                                    self.deg_array.append(shares.deg)
                                    shares.Ω_ref= float(v)
                                    shares.deg_ref= float(x)
                                # self.time_array.append(self.interval*len(self.time_array))
                                print('{:},{:},{:0.0f},{:0.0f},{:0.0f}'.format(self.time_array[self.runs], self.Ω_array[self.runs], self.Ω_ref_array[self.runs], self.deg_ref_array[self.runs],self.deg_array[self.runs] ))
                                # self.L = shares.L #CALCULATED IN CONTROL
                                # print('L: '+ str(self.L))
                                self.runs +=1
                                self.transitionTo(self.S2_DataCollect)
                            if self.runs>= 125:
                                self.transitionTo(self.S3_SendData)
                                
            elif(self.state == self.S3_SendData):
            # Run State 2 Code
                shares.END= True
                self.MotorDriver.disable()
                # print ('ENDED')
                self.transitionTo(self.S0_INIT) 
            
                                
            else:
                        print ('Error: ')
                        
                    
            # self.runs += 1
            
            ## Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
           

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState


# ## Specific pins that the encoder is connected to. Change depending on what pins you used to plug in your encoder.
B6= pyb.Pin.cpu.B6
B7= pyb.Pin.cpu.B7
Timer= 4

## Specific pins that the motor is connected to. Change depending on what pins you used to plug in your encoder.
B4= pyb.Pin.cpu.B4
B5= pyb.Pin.cpu.B5
A15= pyb.Pin.cpu.A15
CH_IN1= 1
CH_IN2= 2
timer= 3

## Creates encoder object specifying which pins the encoder is plugged into and what timer the encoder uses.
enc= EncoderDriver(B6, B7, Timer)

## Import MotorDriver
motor= MotorDriver(A15, B4,CH_IN1, B5, CH_IN2, 3)

## ClosedLoop Driver
closedloop= ClosedLoop(enc, motor)



## Encoder task encodes characters sent from user task
task1 = TaskDataCollection (30, enc, motor)
## User interface task, works with serial port
task2 = Task_Control (enc, motor, closedloop)

while True:
        task1.run()    
        task2.run()    
        
